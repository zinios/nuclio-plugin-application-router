Release Notes
-------------
1.2.1
-----
* Fix issue with similar routes with differing methods.
* Hack does not support break/continue statements with arguments. Fixed this.
* Fixed inconsistencies with the code. Formatted to follow style guide. 


1.2.0
-----
* Depricated Router::handleSimple(). Exact same functionality available in Router::doRoute().

1.1.1
-----
* All exceptions are now throwing proper error codes.

1.1.0
-----
* Routing paths now accept dashes and underscores.
* $args on handleSimple is now nullable, making the argument optional.

1.0.0
-----
* Initial Release.